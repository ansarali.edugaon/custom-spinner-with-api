package com.example.customspinner

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.gson.annotations.SerializedName
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fetchBykesData()
    }

    private fun fetchBykesData(){
        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://demo4346101.mockable.io/")
                .build()

        val service = retrofit.create<BykesSpinnerApi>(BykesSpinnerApi::class.java)
        val call =service.getBykesSpinner()

        call.enqueue(object : Callback<List<BykesSpinnerRecords>> {
            override fun onResponse(
                    call: Call<List<BykesSpinnerRecords>>,
                    response: Response<List<BykesSpinnerRecords>>
            ) {
                val responseList =response.body()
                val spinnerItem = arrayOfNulls<String>(responseList!!.size)
                for ( item in 0 until responseList.size){
                    spinnerItem[item] =responseList.get(item).name
                    spinnerItem[item] =responseList.get(item).image
                }

                val arrayAdapter = ArrayAdapter(this@MainActivity,R.layout.bykes_spinner_item,spinnerItem)
                arrayAdapter.getItemId(R.id.bykes_Spinner_imageView)
                arrayAdapter.getItemId(R.id.bykesName_textView)
                byke_spinner.adapter = arrayAdapter
            }

            override fun onFailure(call: Call<List<BykesSpinnerRecords>>, t: Throwable) {
                Toast.makeText(this@MainActivity,"Some Thing went wrong", Toast.LENGTH_SHORT).show()
            }

        })

    }

    private interface BykesSpinnerApi {
        @GET("byke_spinner")
        fun getBykesSpinner(): Call<List<BykesSpinnerRecords>>
    }


    class BykesSpinnerRecords {
        @SerializedName("name")
        var name:String?=null

        @SerializedName("image")
        var image:String? =null

    }
}